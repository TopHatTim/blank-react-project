import express from 'express';
import path from 'path';

import webpack from 'webpack';

import webpackConfig from '../../config/webpack/livereload.babel.js';

const compiler = webpack(webpackConfig);

function middleware(app) {
  app.use(
    require('webpack-dev-middleware')(compiler, {
      noInfo: true,
      publicPath: webpackConfig.output.publicPath
    })
  );
  app.use(require('webpack-hot-middleware')(compiler));
  app.use('/client', express.static(path.join(__dirname, '../../build/client')));
}

export default middleware;
