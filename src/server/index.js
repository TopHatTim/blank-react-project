import express from 'express';

import routes from './routes';
import middleware from './middleware';

const internalPort = 3000;
const app = express();

function initialise() {
  middleware(app);
  routes(app);
}

try {
  initialise();
  app.listen(internalPort);
} catch (e) {
  console.error(e);
}

export default app;
