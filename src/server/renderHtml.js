import { version } from '../../package.json';

const assetPath = '/client';

export default function renderHtml() {
  return `<!doctype html>
    <html>
      <head>
        <link media="all" rel="stylesheet" href="${assetPath}/app.${version}.bundle.css" />
        <title>Title</title>
        <meta name="viewport" content="initial-scale=1.0">
      </head>
      <body>
        <div id="app">
          Loading ...
        </div>
        <script>
          document.write('<script src="${assetPath}/vendor.${version}.bundle.js"><\\/script>')
          document.write('<script src="${assetPath}/app.${version}.bundle.js"><\\/script>')
        </script>
      </body>
    </html>`;
}
