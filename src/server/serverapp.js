import renderHtml from './renderHtml';

function reactRouteHandler(req, res) {
  res.send(renderHtml());
}

export default reactRouteHandler;
