import React from 'react';
import { render } from 'react-dom';
import './index.css';
import ClientApp from './clientapp';
import registerServiceWorker from './registerServiceWorker';

render(<ClientApp />, document.getElementById('app'));
registerServiceWorker();
