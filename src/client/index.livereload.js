import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ClientApp from './clientapp';
import registerServiceWorker from './registerServiceWorker';

const render = Component => {
  ReactDOM.render(<Component />, document.getElementById('app'));
};

render(ClientApp);

if (module.hot) {
  module.hot.accept('./index.js');
  module.hot.accept('./clientapp', () => {
    render(require('./clientapp').default);
  });
}

registerServiceWorker();
