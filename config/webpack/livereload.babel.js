import { generateWebPackConfig } from './client.babel';
import path from 'path';

const APP_DIR = path.resolve(__dirname, '../../src');

export default generateWebPackConfig({
  environment: 'development',
  liveReload: true,
  devServer: true,
  devtool: 'cheap-module-eval-source-map',
  entryPoint: APP_DIR + '/client/index.livereload.js'
});
