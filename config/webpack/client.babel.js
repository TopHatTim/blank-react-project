import webpack from 'webpack';
import path from 'path';

import pkg, { version } from '../../package.json';

const sourcePath = path.join(__dirname, '../../src');
const staticsPath = path.join(__dirname, '../../build/client');

const generateWebPackConfig = ({
  environment = 'production',
  analyse = false,
  minifify = false,
  liveReload = false,
  devServerPort = 9000,
  devtool = undefined,
  entryPoint = sourcePath + '/client/index.js'
} = {}) => {
  const config = {
    mode: environment,
    context: sourcePath,
    entry: {
      app: (() => {
        return liveReload
          ? [
              'react-hot-loader/patch',
              'webpack-hot-middleware/client',
              entryPoint
            ]
          : [entryPoint];
      })()
    },
    output: {
      path: staticsPath,
      filename: `[name].${version}.bundle.js`,
      publicPath: '/client/'
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          query: {
            presets: ['es2015', 'react'],
            plugins: liveReload
              ? ['transform-class-properties', 'react-hot-loader/babel']
              : undefined
          }
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        },
        {
          test: /\.(jpg|png|svg|gif)$/,
          use: ['file-loader']
        },
        {
          test: /\.(ttf|eot|woff|woff2)$/,
          use: ['file-loader']
        }
      ]
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /react/,
            chunks: 'initial',
            name: 'vendor',
            enforce: true
          }
        }
      }
    },
    plugins: (() => {
      let plugins = [
        // new Dotenv({
        //   path: path.join(__dirname, '../../.env'),
        //   safe: true
        // })
      ];

      liveReload && plugins.push(new webpack.HotModuleReplacementPlugin());
      analyse &&
        plugins.push(
          new Visualizer({
            filename: '../../analysis/js-wvp.html'
          }),
          new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            reportFilename: '../../analysis/js-wba.html'
          })
        );
      minifify &&
        plugins.push(
          new UglifyJsPlugin({
            sourceMap: true
          })
        );

      return plugins;
    })(),
    stats: {
      colors: true
    },
    devtool,
    target: 'web'
  };

  return config;
};

export default generateWebPackConfig();
export { generateWebPackConfig };
